package com.example.testappv4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.CheckBox;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener,
		OnCheckedChangeListener,
		android.widget.CompoundButton.OnCheckedChangeListener {

	LinearLayout mainLayout, newLayout, radioGroupLayout, buttonLayout;
	TextView player, testDisplay;
	EditText pointsEarned;
	CheckBox seens;
	RadioButton wins;
	Button calculate, save;
	EditText p1, p2, p3, p4, p5;
	int[] pt;
	int[] pmt;
	int[] pnt;
	String[] playerName;
	String test = "";
	Intent recv, nextIntent;
	List<CheckBox> seenList;
	List<EditText> pointList;
	List<TextView> result, Payment;
	List<RadioButton> winList;
	ArrayList<String> players;
	TextView total1, total2, total3, total4, total5;
	int seen = 3, unseen = 10, total = 0, playerNo = 5;
	RadioGroup win;
	CheckBox seen1, seen2, seen3, seen4, seen5;
	Map<Integer, ArrayList<Integer>> score;
	// int [][]arr;
	ArrayList<Integer> temp;

	@SuppressLint("UseSparseArrays")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// arr1=new ArrayList<Integer>();
		// arr2=new ArrayList<Integer>();
		// arr3=new ArrayList<Integer>();
		// arr4=new ArrayList<Integer>();
		// arr5=new ArrayList<Integer>();
		score = new HashMap<Integer, ArrayList<Integer>>();
		temp = new ArrayList<Integer>();
		// arr=new int[5][25];
		playerName = new String[5];
		pnt = new int[5];
		pt = new int[5];
		pmt = new int[5];
		players = new ArrayList<String>();
		seenList = new ArrayList<CheckBox>();
		winList = new ArrayList<RadioButton>();
		pointList = new ArrayList<EditText>();
		result = new ArrayList<TextView>();
		Payment = new ArrayList<TextView>();
		testDisplay = (TextView) findViewById(R.id.textView1);
		// win=new RadioGroup(this);
		win = (RadioGroup) findViewById(R.id.radioGroup);
		recv = getIntent();
		Bundle bundle = recv.getExtras();
		players = bundle.getStringArrayList("playerList");
		// testDisplay.setText(text);
		// players=recv.getStringArrayListExtra("playerName");
		playerNo = players.size();
		for (int i = 0; i < playerNo; i++) {
			score.put(i, new ArrayList<Integer>());
		}
		mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
		buttonLayout = new LinearLayout(this);
		buttonLayout.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		buttonLayout.setOrientation(LinearLayout.HORIZONTAL);
		displayLayout();
		// calculate=(Button)findViewById(R.id.calculate);
		// calculate.setOnClickListener(this);
		// win=(RadioGroup)findViewById(R.id.win);
		// win.setOnCheckedChangeListener(this);
		// seen1=(CheckBox)findViewById(R.id.seen1);
		// seen1.setOnCheckedChangeListener(this);
		// seen2=(CheckBox)findViewById(R.id.seen2);
		// seen2.setOnCheckedChangeListener(this);
		// seen3=(CheckBox)findViewById(R.id.seen3);
		// seen4=(CheckBox)findViewById(R.id.seen4);
		// seen5=(CheckBox)findViewById(R.id.seen5);
		// seen3.setOnCheckedChangeListener(this);
		// seen4.setOnCheckedChangeListener(this);
		// seen5.setOnCheckedChangeListener(this);
	}

	void displayLayout() {
		// radioGroupLayout=(LinearLayout)findViewById(R.id.radioGroupLayout);

		final float scale = this.getResources().getDisplayMetrics().density;
		int pixels = (int) (40 * scale + 0.5f);
		for (int i = 0; i < playerNo; i++) {
			wins = new RadioButton(this);
			wins.setId(i);
			wins.setHeight(pixels);
			winList.add(wins);
			win.addView(wins);
		}
		for (int i = 0; i < playerNo; i++) {
			newLayout = new LinearLayout(this);
			newLayout.setLayoutParams(new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			newLayout.setOrientation(LinearLayout.HORIZONTAL);
			player = new TextView(this);
			// if (players.get(i).isEmpty()) {
			// int n = i + 1;
			// player.setText("Player" + n);
			// } else {
			player.setText(players.get(i));
			// }
			player.setTextColor(Color.BLACK);
			player.setTextAppearance(getApplicationContext(),
					R.style.text_style);

			pixels = (int) (50 * scale + 0.5f);
			player.setWidth(pixels);
			newLayout.addView(player);

			seens = new CheckBox(this);
			seens.setId(i);
			seenList.add(seens);
			newLayout.addView(seens);

			pointsEarned = new EditText(this);
			pointsEarned.setWidth(pixels);
			pointsEarned.setId(i);
			pointsEarned.setInputType(InputType.TYPE_CLASS_NUMBER);
			pointsEarned
					.setFilters(new InputFilter[] { new FilterInput(0, 60) });
			pointsEarned.setHint("0-60");
			pointsEarned.setBackgroundResource(R.drawable.rounded_edittext);
			pixels = (int) (60 * scale + 0.5f);
			pointsEarned.setWidth(pixels);
			pointList.add(pointsEarned);

			newLayout.addView(pointsEarned);

			player = new TextView(this);
			player.setText("0");
			player.setTextAppearance(getApplicationContext(),
					R.style.text_style);
			player.setTextColor(Color.BLACK);
			pixels = (int) (50 * scale + 0.5f);
			player.setWidth(pixels);
			player.setId(i);
			result.add(player);
			newLayout.addView(player);

			player = new TextView(this);
			player.setText("0");
			player.setTextColor(Color.BLACK);
			player.setTextAppearance(getApplicationContext(),
					R.style.text_style);
			pixels = (int) (50 * scale + 0.5f);
			player.setWidth(pixels);
			player.setId(i);
			Payment.add(player);
			newLayout.addView(player);

			// wins = new RadioButton(this);
			// win.addView(wins);
			// newLayout.addView(win);
			mainLayout.addView(newLayout);

		}
		calculate = new Button(this);
		calculate.setText("Calculate");
		calculate.setBackgroundResource(R.drawable.red_button);
		calculate.setId(0);
		calculate.setOnClickListener(this);
		buttonLayout.addView(calculate);
		save = new Button(this);
		save.setText("Save");
		save.setId(1);
		save.setBackgroundResource(R.drawable.red_button);
		save.setOnClickListener(this);
		save.setEnabled(false);
		buttonLayout.addView(save);
		mainLayout.addView(buttonLayout);
		// mainLayout.addView(win);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (win.getCheckedRadioButtonId() == -1) {
			Toast.makeText(getApplicationContext(), "Please Select a Winner",
					Toast.LENGTH_LONG).show();

		} else {
			int i;
			// if (calculate.getText().toString().compareTo("Calculate") == 0) {
			if (v.getId() == 0) {
				this.total = 0;
				// testDisplay.setText(String.valueOf(total));
				for (i = 0; i < playerNo; i++) {
					if (seenList.get(i).isChecked() || win.getCheckedRadioButtonId()==winList.get(i).getId()) {
						try {
							pt[i] = Integer.parseInt(pointList.get(i).getText()
									.toString());
						} catch (NumberFormatException e) {
							Toast.makeText(getApplicationContext(),
									"One or more score is missing",
									Toast.LENGTH_SHORT).show();
							// pt[i]=0;
							return;

						}

					} else {
						pt[i] = 0;
					}
					this.total += pt[i];
				}
				// testDisplay.setText(String.valueOf(total));
				// //String total;
				// p1=(EditText)findViewById(R.id.point1);
				// p2=(EditText)findViewById(R.id.point2);
				// p3=(EditText)findViewById(R.id.point3);
				// p4=(EditText)findViewById(R.id.point4);
				// p5=(EditText)findViewById(R.id.point5);
				// this.pt1+=Integer.parseInt(p1.getText().toString());
				// this.pt2+=Integer.parseInt(p2.getText().toString());
				// this.pt3+=Integer.parseInt(p3.getText().toString());
				// this.pt4+=Integer.parseInt(p4.getText().toString());
				// this.pt5+=Integer.parseInt(p5.getText().toString());
				// total1=(TextView)findViewById(R.id.total1);
				// total2=(TextView)findViewById(R.id.total2);
				// total3=(TextView)findViewById(R.id.total3);
				// total4=(TextView)findViewById(R.id.total4);
				// total5=(TextView)findViewById(R.id.total5);
				// this.total=this.pt1+this.pt2+this.pt3+this.pt4+this.pt5;
				this.calculatePenenty();
				this.calculatePayment();
				this.setPayment();
				save.setEnabled(true);
			}
			// else if (calculate.getText().toString().compareTo("Save") == 0) {
			else if (v.getId() == 1) {
				nextIntent = new Intent(getApplicationContext(),
						ScoreCard.class);
				for (i = 0; i < playerNo; i++) {
					score.get(i).add(pmt[i]);
					nextIntent.putIntegerArrayListExtra("Score" + i,
							score.get(i));
				}
				nextIntent.putStringArrayListExtra("playerList", players);
				startActivity(nextIntent);

			}
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if (isChecked) {

		}

	}

	// @Override
	// protected void onResume() {
	// mainLayout.removeAllViews();
	// win.removeAllViews();
	// buttonLayout.removeAllViews();
	// super.onResume();
	// displayLayout();
	// }

	public void calculatePenenty() {
		// this.pnt1=this.pnt2=this.pnt3=this.pnt4=this.pnt5=0;
		// testDisplay.setText(String.valueOf(win.getCheckedRadioButtonId()));
		for (int i = 0; i < playerNo; i++) {
			pnt[i] = 0;
		}
		for (int i = 0; i < playerNo; i++) {
			if (seenList.get(i).isChecked()) {
				if (win.getCheckedRadioButtonId() == winList.get(i).getId()) {
					pnt[i] = 0;
				} else {
					pnt[i] = this.seen;
				}
			} else {
				pnt[i] = this.unseen;

			}
			// testDisplay.setText(String.valueOf(pnt[i]));
			// for ( i = 0; i < 1000000000; i++) ;
		}
		// testDisplay.setText(String.valueOf(pnt[1]));
		// if(seen1.isChecked()){
		// if(win.getCheckedRadioButtonId()==R.id.win1){
		// this.pnt1=0;
		// }else{
		// this.pnt1=this.seen;
		// }
		// }
		// else{
		// this.pnt1=this.unseen;
		// }
		// if(seen2.isChecked()){
		// if(win.getCheckedRadioButtonId()==R.id.win2){
		// this.pnt2=0;
		// }else{
		// this.pnt2=this.seen;
		// }
		// }
		// else{
		// this.pnt2=this.unseen;
		// }
		// if(seen3.isChecked()){
		// if(win.getCheckedRadioButtonId()==R.id.win3){
		// this.pnt3=0;
		// }else{
		// this.pnt3=this.seen;
		// }
		// }
		// else{
		// this.pnt3=this.unseen;
		// }
		// if(seen4.isChecked()){
		// if(win.getCheckedRadioButtonId()==R.id.win4){
		// this.pnt4=0;
		// }else{
		// this.pnt4=this.seen;
		// }
		// }
		// else{
		// this.pnt4=this.unseen;
		// }
		// if(seen5.isChecked()){
		// if(win.getCheckedRadioButtonId()==R.id.win5){
		// this.pnt5=0;
		// }else{
		// this.pnt5=this.seen;
		// }
		// }
		// else{
		// this.pnt5=this.unseen;
		// }
	}

	public void calculatePayment() {
		int i, j;
		for (i = 0; i < playerNo; i++) {
			pmt[i] = 0;
		}
		// testDisplay.setText(String.valueOf(pnt[1]));
		for (i = 0; i < playerNo; i++) {
			if (winList.get(i).getId() != win.getCheckedRadioButtonId()) {
				pmt[i] = pt[i] * playerNo - total - pnt[i];
			}
		}
		for (i = 0; i < playerNo; i++) {
			if (win.getCheckedRadioButtonId() == winList.get(i).getId()) {
				// testDisplay.setText("Player"+i+"won");
				pmt[i] = 0;
				for (j = 0; j < playerNo; j++) {
					if (i != j) {
						pmt[i] += -pmt[j];
					}

				}
			}
			// testDisplay.setText(String.valueOf(pmt[i]));
		}

		// testDisplay.setText(String.valueOf(pmt[0]));
		// this.pmt1=this.pmt2=this.pmt3=this.pmt4=this.pmt5=0;
		// switch(win.getCheckedRadioButtonId()){
		// case R.id.win1:
		// this.pmt2=this.pt2*this.playerNo-this.total-this.pnt2;
		// this.pmt3=this.pt3*this.playerNo-this.total-this.pnt3;
		// this.pmt4=this.pt4*this.playerNo-this.total-this.pnt4;
		// this.pmt5=this.pt5*this.playerNo-this.total-this.pnt5;
		// this.pmt1=-this.pmt2-this.pmt3-this.pmt4-this.pmt5;
		// break;
		//
		// case R.id.win2:
		// this.pmt1=this.pt1*this.playerNo-this.total-this.pnt1;
		// this.pmt3=this.pt3*this.playerNo-this.total-this.pnt3;
		// this.pmt4=this.pt4*this.playerNo-this.total-this.pnt4;
		// this.pmt5=this.pt5*this.playerNo-this.total-this.pnt5;
		// this.pmt2=-this.pmt1-this.pmt3-this.pmt4-this.pmt5;
		//
		// break;
		//
		// case R.id.win3:
		// this.pmt2=this.pt2*this.playerNo-this.total-this.pnt2;
		// this.pmt1=this.pt1*this.playerNo-this.total-this.pnt1;
		// this.pmt4=this.pt4*this.playerNo-this.total-this.pnt4;
		// this.pmt5=this.pt5*this.playerNo-this.total-this.pnt5;
		// this.pmt3=-this.pmt2-this.pmt1-this.pmt4-this.pmt5;
		// break;
		//
		// case R.id.win4:
		// this.pmt2=this.pt2*this.playerNo-this.total-this.pnt2;
		// this.pmt3=this.pt3*this.playerNo-this.total-this.pnt3;
		// this.pmt1=this.pt1*this.playerNo-this.total-this.pnt1;
		// this.pmt5=this.pt5*this.playerNo-this.total-this.pnt5;
		// this.pmt4=-this.pmt2-this.pmt3-this.pmt1-this.pmt5;
		//
		// break;
		//
		// case R.id.win5:
		// this.pmt2=this.pt2*this.playerNo-this.total-this.pnt2;
		// this.pmt3=this.pt3*this.playerNo-this.total-this.pnt3;
		// this.pmt4=this.pt4*this.playerNo-this.total-this.pnt4;
		// this.pmt1=this.pt1*this.playerNo-this.total-this.pnt1;
		// this.pmt5=-this.pmt2-this.pmt3-this.pmt4-this.pmt1;
		// break;
		//
		// }
	}

	public void setPayment() {
		for (int i = 0; i < playerNo; i++) {
			result.get(i).setText(String.valueOf(pmt[i]));
			// temp = score.get(i);
			// temp.add(pmt[i]);
			// score.put(i, temp);
		}
		// testDisplay.setText(score.get(0).toString());
		// temp = score.get(0);
		// temp.add(pmt[0]);
		// score.remove(0);
		// score.put(0, temp);
		// score.get(0).add(pmt[0]);
		// score.get(1).add(pmt[1]);
		// score.get(2).add(pmt[2]);
		// total1.setText(String.valueOf(this.pmt1));
		// total2.setText(String.valueOf(this.pmt2));
		// total3.setText(String.valueOf(this.pmt3));
		// total4.setText(String.valueOf(this.pmt4));
		// total5.setText(String.valueOf(this.pmt5));
	}

}
