package com.example.testappv4;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.drm.DrmStore.Action;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PlayerSelection extends Activity implements OnClickListener {

	TextView txtPlayer, testDisplay;
	EditText myEditText, playerNo;
	Button b;
	LinearLayout myLayout, hlayout;
	List<EditText> myedittext;
	ArrayList<String> playerLists;
	// String[] playerList;
	int player;
	String test = "";
	Intent nextIntent;
	int maxLength;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player_selection);
		myedittext = new ArrayList<EditText>();
		playerLists = new ArrayList<String>();
		// playerList = new String[5];
		playerNo = (EditText) findViewById(R.id.playerNo);
		playerNo.setHint("2-5");
		playerNo.requestFocus();
		playerNo.setFilters(new InputFilter[] { new FilterInput("2", "5") });
		testDisplay = (TextView) findViewById(R.id.textView1);
		b = (Button) findViewById(R.id.submit);
		myLayout = (LinearLayout) findViewById(R.id.playerNames);
		b.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.player_selection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (b.getText().toString().compareTo("Submit") == 0) {
			try{
			player = Integer.parseInt(playerNo.getText().toString());
			} catch (NumberFormatException e) {
				Toast.makeText(getApplicationContext(),
						"Please Enter Number of Player",
						Toast.LENGTH_SHORT).show();
				// pt[i]=0;
				return;

			}
			
			b.setText("Continue");
			int i = 0;
			txtPlayer = new TextView(getApplicationContext());
			txtPlayer.setTextAppearance(getApplicationContext(), R.style.text_style);
			txtPlayer.setText("Enter Names of Players");
			 txtPlayer.setTextColor(Color.BLACK);
			myLayout.addView(txtPlayer);
			for (i = 0; i < player; i++) {
				final float scale = this.getResources().getDisplayMetrics().density;
				int pixels = (int) (70 * scale + 0.5f);
				hlayout = new LinearLayout(getApplicationContext());
				hlayout.setLayoutParams(new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				hlayout.setOrientation(LinearLayout.HORIZONTAL);
				txtPlayer = new TextView(getApplicationContext());
				txtPlayer.setWidth(pixels);
				txtPlayer.setTextAppearance(getApplicationContext(), R.style.text_style);
				//pixels = (int) (35 * scale + 0.5f);
				//txtPlayer.setHeight(pixels);
				txtPlayer.setText("Player" + String.valueOf(i + 1));
				// txtPlayer.setTextSize(50);
				 txtPlayer.setTextColor(Color.BLACK);
				hlayout.addView(txtPlayer);
				myEditText = new EditText(getApplicationContext());
				pixels = (int) (120 * scale + 0.5f);
				myEditText.setWidth(pixels);
//				pixels = (int) (25 * scale + 0.5f);
//				myEditText.setHeight(pixels);
				myEditText.setId(i);
				myEditText.setTextColor(Color.BLACK);
				myEditText.setBackgroundResource(R.drawable.rounded_edittext);
				myedittext.add(myEditText);
				myEditText.setSingleLine();
				//myEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
				maxLength = 7;
				InputFilter[] fArray = new InputFilter[1];
				fArray[0] = new InputFilter.LengthFilter(maxLength);
				myEditText.setFilters(fArray);
				hlayout.addView(myEditText);
				myLayout.addView(hlayout);

			}

			// for (i = 0; i < player; i++) {
			// test += myedittext.get(i).getText().toString();
			//
			// }
			// testDisplay.setText(test);
		} else if (b.getText().toString().compareTo("Continue") == 0) {
			for (int i = 0; i < player; i++) {
				// playerList[i] = myedittext.get(i).getText().toString();
				if (myedittext.get(i).getText().toString().compareTo("") == 0) {
					int n = i + 1;
					playerLists.add("P" + n);
				} else {
					playerLists.add(myedittext.get(i).getText().toString());
					// test+=myedittext.get(i).getText().toString();
				}
			}
			// testDisplay.setText(playerList[0]);
			Intent nextIntent = new Intent(getApplicationContext(),
					MainActivity.class);
			nextIntent.putStringArrayListExtra("playerList", playerLists);
			nextIntent.putExtra("playerNo", player);
			// nextIntent.put
			startActivity(nextIntent);

		}

	}
}
