package com.example.testappv4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScoreCard extends Activity {

	TextView display;
	LinearLayout mainLayout, scoreCard, round, newLayout;
	Intent recv;
	int playerNo, recordNo;
	Map<Integer, ArrayList<Integer>> score;
	ArrayList<Integer> temp;
	int[] scoreTotal;
	ArrayList<String> PlayerName;

	
	@SuppressLint("UseSparseArrays")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		int i;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_score_card);
		PlayerName = new ArrayList<String>();
		score = new HashMap<Integer, ArrayList<Integer>>();
		scoreTotal = new int[5];
		mainLayout = (LinearLayout) findViewById(R.id.playerNames);
		scoreCard = (LinearLayout) findViewById(R.id.scores);
		round = (LinearLayout) findViewById(R.id.round);
		recv = getIntent();
		Bundle bundle = recv.getExtras();
		PlayerName = bundle.getStringArrayList("playerList");
		playerNo = PlayerName.size();
		// display.setText(String.valueOf(playerNo));
		for (i = 0; i < playerNo; i++) {
			temp = bundle.getIntegerArrayList("Score" + i);
			score.put(i, temp);
			scoreTotal[i] = 0;
		}
		recordNo = score.get(0).size();
		displayLayout();
	}

	void displayLayout() {
		int i, n, j;
		final float scale = this.getResources().getDisplayMetrics().density;
		int pixels = (int) (50 * scale + 0.5f);

		for (i = 0; i < playerNo; i++) {

			display = new TextView(this);
			display.setText(PlayerName.get(i));
			display.setTextAppearance(getApplicationContext(), R.style.text_style);
			display.setWidth(pixels);
			mainLayout.addView(display);
		}
		// display=new TextView(this);
		// display.setText(score.get(0).toString());
		// round.addView(display);
		for (i = 0; i < recordNo; i++) {
			n = i + 1;
			display = new TextView(this);
			display.setTextAppearance(getApplicationContext(), R.style.text_style);
			display.setText(String.valueOf(n));
			round.addView(display);
		}
		for (i = 0; i < recordNo; i++) {
			newLayout = new LinearLayout(this);
			newLayout.setLayoutParams(new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			newLayout.setOrientation(LinearLayout.HORIZONTAL);
			for (j = 0; j < playerNo; j++) {
				display = new TextView(this);
				display.setText(score.get(j).get(i).toString());
				display.setTextAppearance(getApplicationContext(), R.style.text_style);
				scoreTotal[j] += Integer.parseInt(score.get(j).get(i)
						.toString());
				display.setWidth(pixels);
				newLayout.addView(display);
			}
			scoreCard.addView(newLayout);
		}
		newLayout = new LinearLayout(this);
		newLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
		newLayout.setOrientation(LinearLayout.HORIZONTAL);
		display = new TextView(this);
		display.setText("Total");
		display.setTextAppearance(getApplicationContext(), R.style.text_style);
		display.setWidth(pixels);
		round.addView(display);
		for (i = 0; i < playerNo; i++) {
			display = new TextView(this);
			display.setText(String.valueOf(scoreTotal[i]));
			display.setTextAppearance(getApplicationContext(), R.style.text_style);
			display.setWidth(pixels);
			newLayout.addView(display);

		}
		scoreCard.addView(newLayout);// scoreCard.addView(newLayout);

	}
	
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.score_card, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
